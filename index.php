<?php
require __DIR__ . '/bootstrap.php';
?>
<!-- Header -->
<?php include_once(__DIR__ . '/resources/header.html'); ?>

<!-- Body -->
 <?php include_once(__DIR__ . '/resources/index.html'); ?>

  <!-- Footer -->
  <?php include_once(__DIR__ . '/resources/footer.html'); ?>
