// Document ready
$(document).ready(function () {
    loadDB(true);
});

// Search API request, save on success probability > 0.51%
$('#search_btn').on('click', function () {
    var searchName = $("#search_input").val();
    if (searchName == '' || searchName.length < 3) {
        alert('Enter valid name consisting of 3+ characters!');
        return false;
    }

    $.ajax({
        url: 'Api/api.php',
        method: 'POST',
        data: {
            search: searchName,
        }
    })
    .done(function (responseApi) {
        // console.log(responseApi);
        if (responseApi.eligible) {
            // Modal show
            $('#input-data').show();
            $('#no-data').hide();
            $('#dataModal #data-name').val(responseApi.name);
            $('#dataModal #data-gender').val(responseApi.gender);
            $('#dataModal #data-probability').val(responseApi.probability);
            $('#dataModal #data-count').val(responseApi.count);
            $('#dataModal').modal('show');
            jQuery('html,body').animate({scrollTop:0},500);

            // Save to Database
            $.ajax({
                url: 'Api/database.php',
                method: 'POST',
                data: {
                    name: responseApi.name,
                    gender: responseApi.gender,
                    probability: responseApi.probability,
                    count: responseApi.count,
                    save: true
                }
                }).done(function (responseSave) {
                    // console.log(responseSave, typeof(responseSave), responseSave);
                    if (responseSave.success) {
                        $('.table-striped #tbody_data').append(
                            '<tr><td>' + responseSave.lastID + '</td>' +
                            '<td>' + responseApi.name + '</td>' +
                            '<td>' + responseApi.gender + '</td>' +
                            '<td>' + responseApi.probability + '</td>' +
                            '<td>' + responseApi.count + '</td></tr>'
                        );
                        alert('Data Saved');
                    } else {
                        alert('Could not save, ' + responseSave.message);
                    }

                }).fail(function (error) {
                    // console.log(error);
                }).always(function (message) {
                    // console.log(message);
                });
        }

        if (!responseApi.eligible) {
            alert('The Name is not Eligible, has score less than 0.51%, (' + responseApi.probability + ')');
        }

        $('#container-loader').hide();  // hide the loader
    })
    .fail(function (responseApi) {
        // console.log(responseApi);
    })
    .always(function (responseApi) {
        // console.log(responseApi);
    });
// }
});

// Database data loader
// $('#data_loader').on('click', function(){

// });

function loadDB(init = false)
{
    // console.log(init);
    $.ajax({
        url: 'Api/database.php',
        method: 'POST',
        data: {
            initialize: init,
            save: false
        }
        })
        .done(function (responseLoad) {
            // console.log(responseLoad);
            $('#input-data').show();
            $('#no-data').hide();
            if (responseLoad['load'] === true) {
                $(responseLoad['data']).each(function (index, element) {
                    $('.table-striped #tbody_data').append(
                        '<tr><td>' + element.id + '</td>' +
                        '<td>' + element.firstName + '</td>' +
                        '<td>' + element.gender + '</td>' +
                        '<td>' + element.probability + '</td>' +
                        '<td>' + element.counter + '</td></tr>'
                    );
                });
                jQuery('html,body').animate({scrollTop:0},500);
            } else {
                alert('No New Data');
                $('#container-loader').hide();  // hide the loader
            }

            $('#container-loader').hide();  // hide the loader
        }).fail(function (error) {
            // console.log(error);
        }).always(function (message) {
            // console.log(message);
        });
}

// Scroll To Top
function goToTop()
{
    jQuery('html,body').animate({scrollTop:0},700);
}
