<?php

header("Content-type: application/json; charset=utf-8");

require __DIR__ . '/../bootstrap.php';

use Classes\Database;

$database = new Database();

// Only POST requests are accepted
if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
    die('Only POST requests are accepted');
}

// Save to DB
if ($_POST['save'] === "true") {
    if (isset($_POST['name']) && isset($_POST['gender']) && isset($_POST['probability']) && isset($_POST['count'])) {
        $post['name'] = $_POST['name'];
        $post['gender'] = $_POST['gender'];
        $post['probability'] = $_POST['probability'];
        $post['count'] = $_POST['count'];

        // Save record
        $lastID = $database->save($post);

        if ($lastID === null) {
            http_response_code(202);
            echo json_encode([
                'success' => false,
                'message' => 'Name already exists'
            ]);
            return;
        }

        // Invalid name
        if (!$lastID) {
            http_response_code(202);
            echo json_encode([
                'success' => false,
                'message' => 'Invalid name'
            ]);
            return;
        }

        // Success
        http_response_code(201);
        echo json_encode([
            'success' => true,
            'lastID' => $lastID
        ]);
    }

    return;
}

// Load data
if ($_POST['save'] === "false") {
    $initialize = $_POST['initialize'] ?? false;

    $lastID = $database->getLastRecordId();

    // Initial loading all data
    if ($initialize === "true") {
        $data = $database->all();
        // $_SESSION['load'] = true;
        $_SESSION['lastID'] = $lastID;
        $_SESSION['initialized'] = true;

        http_response_code(200);
        echo json_encode([
            'data' => $data,
            'lastID' => $lastID,
            'load' => true,
            'initialized' => $initialize

        ]);
        return;
    }

    // No new data, no init
    if (isset($_SESSION['lastID']) && $_SESSION['lastID'] === $lastID) {
        // $_SESSION['load'] = false;

        http_response_code(200);
        echo json_encode([
            'data' => [],
            'lastID' => $lastID,
            'load' => false,
            'session' => 'we got session lastID'
        ]);
        return;
    }

    // Get last record data && set session && append only the {new data}
    $data = $database->getLastRecord();
    if ($_SESSION['lastID'] < $data['id']) {
        $_SESSION['lastID'] = $data['id'];
        $_SESSION['load'] = true;

        http_response_code(200);
        echo json_encode([
            'data' => $data,
            'lastID' => $lastID,
            'load' => true,
            'session' => 'all data'
        ]);
        // return;
    }
}
