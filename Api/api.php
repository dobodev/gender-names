<?php

header("Content-type: application/json; charset=utf-8");

// session_start();
require __DIR__ . '/../bootstrap.php';

use Classes\Api;

// Only POST requests are accepted
if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
    die('Only POST requests are accepted');
}

$api = new Api();

$searchName = $_POST['search'];

$arr = explode(' ', $searchName);
$response = $api->requestGenderizeIO($arr);

// Http response
http_response_code(200);
echo json_encode($response);
