<?php

namespace Classes;

use Classes\Validator;

class Database
{
    private string $driver;
    private string $host;
    private string $database;
    private string $port;
    private string $user;
    private string $password;
    private string $dsn;

    /** @var array<mixed> */
    private array $pdo_options;
    private \PDO $connection;
    // private $sourceDBTable;
    protected static string $table;

    public function __construct()
    {
        $this->driver = $_ENV['DB_CONNECTION_DRIVER'];
        $this->host = $_ENV['DB_HOST'];
        $this->database = $_ENV['DB_DATABASE'];
        $this->user = $_ENV['DB_USER'];
        $this->password = $_ENV['DB_PASSWORD'];
        $this->port = $_ENV['DB_PORT'];
        self::$table = 'filtered_names';

        $this->dsn = $this->driver . ':dbname=' . $this->database . ';host=' . $this->host .
        ';port=' . $this->port . ';charset=utf8';

        $this->pdo_options = [
            \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION,
            \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC,
            \PDO::ATTR_EMULATE_PREPARES   => false,
        ];

        // Set the DB connection
        $this->setConnection();
    }

    /**
     * Set the DB connection
     */
    public function setConnection(): void
    {
        try {
            $this->connection = new \PDO($this->dsn, $this->user, $this->password, $this->pdo_options);
        } catch (\PDOException $e) {
            error_log("Database Connection Error: " . $e->getMessage());
            die();
        }
    }

    /**
     * Get table name
     * @return string
     */
    protected static function getTable(): string
    {
        return self::$table;
    }

    /**
     * Get DB connection
     * @return \PDO
     */
    public function getConnection(): \PDO
    {
        return $this->connection;
    }

    /**
     * Get the last record ID from the table
     * @return int|string
     */
    public function getLastRecordId(): int|string
    {
        $sql = "SELECT id FROM {$this::getTable()} ORDER BY id DESC LIMIT 1";
        $id = $this->getConnection()->query($sql)->fetch(\PDO::FETCH_COLUMN, 0);

        return $id;
    }

     /**
     * Get the last record
     * @return array<mixed> array
     */
    public function getLastRecord(): array
    {
        $sql = "SELECT id, firstName, gender, probability, counter
            FROM {$this::getTable()}
            ORDER BY id DESC
            LIMIT 1";

        $lastRecord = $this->getConnection()->query($sql, \PDO::FETCH_ASSOC);

        return $lastRecord->fetch(\PDO::FETCH_ASSOC);
    }

    /**
     * Get data to be displayed
     * @return array<mixed> array
     */
    public function all(): array
    {
        // $dbSource = $this->setSourceDBTable('filtered_names');

        $sql = "SELECT id, firstName, gender, probability, counter FROM {$this::getTable()}";
        $result = $this->getConnection()->query($sql, \PDO::FETCH_ASSOC);

        return $result->fetchAll();
    }

    /**
     * Save record
     * @param array<mixed> $post
     * @return bool | string | null
     */
    public function save(array $post): bool|string|null
    {
        // Validate name
        if (!$this->validName($post['name'])) {
            return false;
        }

        // Name exists
        if ($this->exists($post['name'])) {
            return null;
        }

        try {
            $sql = "INSERT INTO {$this::getTable()} (firstName, gender, probability, counter, timestamp)
                          VALUES(:name, :gender, :probability, :count, now())";

            $pdoStatement = $this->connection->prepare($sql);

            $dataSaved = $pdoStatement->execute([
                                         ':name' => $post['name'],
                                         ':gender' => $post['gender'],
                                         ':probability' => $post['probability'],
                                         ':count' => $post['count']
                                        ]);
            if ($dataSaved) {
                return $this->connection->lastInsertId();
            }
        } catch (\Throwable $th) {
            error_log("Error saving to database: " . $th->getMessage());
            echo json_encode(["Error" => "Error saving to database"]);
        }

        return false;
    }

    /**
     * Check if name exists
     * @param string $name
     * @return bool
     * @throws \PDOException
     */
    public function exists($name)
    {
        $sql = "SELECT id FROM {$this::getTable()} WHERE firstName = :name";
        $stmt = $this->getConnection()->prepare($sql);
        $stmt->execute([':name' => $name]);

        return $stmt->rowCount() > 0;
    }

    /**
     * Validate name
     * @param string $name
     * @return bool
     */
    public function validName(string $name): bool
    {
        $validator = new Validator();

        if (!$validator->validateString($name)) {
            return false;
        }

        return true;
    }
}
