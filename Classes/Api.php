<?php

namespace Classes;

class Api
{
/**
     * Api request
     * @param array<string> $chunk
     * @return array<mixed>
     */
    public function requestGenderizeIO($chunk): array
    {
        $apiString = '';
        foreach ($chunk as $name_string) {
            $apiString .= 'name[]=' . $name_string . '&';
        }

        $api_endpoint = $_ENV['API_GENDER_URI'] . "?$apiString";
        $curl = curl_init($api_endpoint);

        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $result = curl_exec($curl);

        curl_close($curl);

        $eligibleResponse = $this->checkProbability($result);

        return $eligibleResponse;
    }

    /**
     * Check probability criteria
     * @param bool|string $apiResponse
     * @return array<mixed>
     */
    private function checkProbability($apiResponse): array
    {
        $apiResponseAsArray = json_decode($apiResponse, true);
        $apiResponseAsArray = $apiResponseAsArray[0];

        $apiResponseAsArray['eligible'] = false; // default value
        if ($apiResponseAsArray['probability'] > 0.5) {
            $apiResponseAsArray['eligible'] = true;
        }

        return $apiResponseAsArray;
    }
}
