<?php

namespace Classes;

use Classes\Database;

class ExportCSV
{
    /**
     * Export CSV
     * @param string $fileName
     * @return void
     */
    public static function export(string $fileName): void
    {
        /** @var Database */
        $database = new Database();

        $records = $database->all();

        $header = [];
        foreach ($records as $key => $value) {
            $header[] = array_keys($value);
            break;
        }

        $outputStream = fopen('php://output', 'w');
        header('Content-type: application/csv');
        header('Content-Disposition: attachment; filename=' . $fileName);

        fputcsv($outputStream, $header[0]); // header row

        foreach ($records as $arr) {
            fputcsv($outputStream, $arr);
        }

        fclose($outputStream);
    }
}
