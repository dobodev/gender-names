<?php

namespace Classes;

class Validator
{
    /**
     * Check if a string is not empty
     *
     * @param string $value
     * @return bool
     */
    public function isNotEmpty(string $value): bool
    {
        return !empty(trim($value));
    }

    /**
     * Check if a string does not contain illegal characters
     *
     * @param string $value
     * @return bool
     */
    public function hasNoIllegalCharacters(string $value): bool
    {
        // Define the pattern for allowed characters (alphanumeri)
        $pattern = "/^[a-zA-Z]+$/";
        return preg_match($pattern, $value) === 1;
    }

    /**
     * Check if a string has a length between min and max
     *
     * @param string $value
     * @param int $min
     * @param int $max
     * @return bool
     */
    public function isLengthBetween(string $value, int $min, int $max): bool
    {
        $length = strlen($value);
        return $length >= $min && $length <= $max;
    }

    /**
     * Validate the string with all criteria
     *
     * @param string $value
     * @return bool
     */
    public function validateString(string $value): bool
    {
        return $this->isNotEmpty($value) &&
               $this->hasNoIllegalCharacters($value) &&
               $this->isLengthBetween($value, 3, 25);
    }
}
