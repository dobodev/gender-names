<?php

declare(strict_types=1);

namespace Tests\Unit;

require_once __DIR__ . '/../../bootstrap.php';

use Classes\Database;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;

final class DatabaseTest extends TestCase
{
    protected $databaseMocker;

    protected $databaseStub;
    public function setUp(): void
    {
        $this->databaseMocker = $this->createMock(Database::class);

        $this->databaseStub = $this->createStub(Database::class);
    }

    public function tearDown(): void
    {
        unset($this->databaseMocker);
        unset($this->databaseStub);
    }

    public function testGetLastRecord(): void
    {
        // Database Stubbing

        $this->databaseStub->method('getLastRecord')
            ->willReturn([['id' => 101, 'name' => 'Mark', 'gender' => 'male']]);

        $this->assertSameSize(
            [['id' => 101, 'name' => 'Mark', 'gender' => 'male']],
            $this->databaseStub->getLastRecord()
        );
    }

    public function testLastRecordNotNull(): void
    {
        // Database mocking
        $this->databaseMocker->expects($this->once())
            ->method('getLastRecord')
            ->willReturn([['id' => 101, 'name' => 'Mark', 'gender' => 'male']]);

        $this->assertNotEmpty($this->databaseMocker->getLastRecord());
    }

    public function testGetAll(): void
    {
        $this->assertIsArray($this->databaseMocker->all());
    }

    /**
     * Data provider array for the testValidName method
     * @return array
     */
    public static function inValidNames(): array
    {
        return [
            ['123123', false],
            ['mark123', false],
            ['@#2dfdsMark', false],
            ['s23mith', false],
        ];
    }

    #[DataProvider('inValidNames')]
    public function testValidName(string $name, bool $expected): void
    {
        $this->assertFalse($this->databaseMocker->validName($name));
    }
}
