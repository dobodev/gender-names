<?php

namespace Tests\Integration;

require_once __DIR__ . '/../../bootstrap.php';

use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\Attributes\DataProvider;
use Classes\Api;

class ApiTest extends TestCase
{
    private Api $api;
    /**
     * Create a new instance of the Api class
     * @return void
     */
    public function setUp(): void
    {
        // Create a new instance of the Api class
        $this->api = new Api();
    }

    /**
     * Data provider array for the testRequestGenderizeIOResponse method
     * @return array
     */
    public static function dataProviderTestTrueRequestGenderizeIO(): array
    {
        return [
            ['Hector', true],
            ['jenna', true],
            ['Jenna', true],
            ['Mike', true],
            ['Jason', true],
            ['123123', false],
            ['mark123', false],
            ['123123', false],
            ['mark123', false],
            ['@#2dfdsMark', false],
            ['s23mith', false],
        ];
    }

    /**
     * Test the requestGenderizeIO method of the Api class
     *
     * @return void
     */
    #[DataProvider('dataProviderTestTrueRequestGenderizeIO')]
    public function testRequestGenderizeIOResponse(string $name, bool $probability)
    {
        // Send a request to the genderize.io API with a name parameter
        $response = $this->api->requestGenderizeIO([$name]);

        $this->assertSame($probability, $response['eligible']);
    }
}
